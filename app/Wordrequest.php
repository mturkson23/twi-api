<?php namespace App;
  
use Illuminate\Database\Eloquent\Model;
  
class Wordrequest extends Model
{
     protected $table = 'tb_wordrequest';

     protected $fillable = ['word','translation','photourl'];
}
?>