<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $table = 'tb_country';
}