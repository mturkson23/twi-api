<?php

namespace App\Http\Controllers;
use App\Wordrequest;
use App\Country;

use Illuminate\Http\Request;

class WordrequestController extends Controller
{
    public function index(){
        $Wordrequests  = Wordrequest::all();
        return response()->json($Wordrequests);
    }
  
    public function getWordrequest($id){
        $Wordrequest  = Wordrequest::find($id);
        return response()->json($Wordrequest);
    }
  
    public function createWordrequest(Request $request){
        $Wordrequest  = new Wordrequest;
        $Wordrequest->word = $request->input('word');
        $Wordrequest->translation = $request->input('translation');
        // $Wordrequest->photourl = $request->input('photourl');
        $ip = $request->ip();
        $res = file_get_contents("https://www.iplocate.io/api/lookup/$ip");
        $resjson = json_decode($res);
        if (!is_null($resjson->country)){
            $country = Country::where('recname', $resjson->country)->first();
            $Wordrequest->countryid = $country->id;
        }
        //  check for a valid file
        if($request->has('img')){
		// generate token number
		$year = str_pad((string)(intval(date('Y'))-2000),2,'0');
            // get institutional code
            $inst = 'ksnm';
            // get recent token
            $lasttoken = Wordrequest::where(['status'=>1])->orderBy('created_at','desc')->pluck('id')->first();
            if(is_null($lasttoken)){
                $seq = str_pad('1',3,'0',STR_PAD_LEFT);
            }else{
                $seq = str_pad((string)($lasttoken+1), 3, '0', STR_PAD_LEFT);
            }
            // dd($seq);
            $uniq = ((98 - ((intval($year . $seq)*100) % 97)) % 97);
            $code = $inst . $year . $seq . str_pad((string)$uniq,2,'0',STR_PAD_LEFT);

            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('img')));
            $saved = file_put_contents(public_path('requests')."/$code.png", $data); 
            if($saved!==FALSE) $Wordrequest->photourl = "$code.png";    
            // $path = $request->img->storeAs('requests', "$code.png");
        }
        $Wordrequest->ipaddr = $ip;
        $Wordrequest->locdata = $res;
        $Wordrequest->save();
        return response()->json($Wordrequest);
    }
  
    public function deleteWordrequest($id){
        $Wordrequest  = Wordrequest::find($id);
        $Wordrequest->delete();
        return response()->json('deleted');
    }
  
    public function updateWordrequest(Request $request,$id){
        $Wordrequest  = Wordrequest::find($id);

        $Wordrequest->word = $request->input('word');
        $Wordrequest->translation = $request->input('translation');
        $Wordrequest->photourl = $request->input('photourl');
        $ip = $request->ip();
        $res = file_get_contents("https://www.iplocate.io/api/lookup/$ip");
        $resjson = json_decode($res);
        if (!is_null($resjson->country)){
            $country = Country::where('recname', $resjson->country)->first();
            $Wordrequest->countryid = $country->id;
        }      
        $Wordrequest->ipaddr = $ip;
        $Wordrequest->locdata = $res;
        $Wordrequest->save();
        return response()->json($Wordrequest);
    }
}
