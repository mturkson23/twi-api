<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Nothing to see here. Move along';
});

$router->group(['prefix'=>'api/v1'], function($app)
{
    $app->get('request','WordrequestController@index');
    $app->get('request/{id}','WordrequestController@getWordrequest');
    $app->post('request','WordrequestController@createWordrequest');
    $app->put('request/{id}','WordrequestController@updateWordrequest');
    $app->delete('request/{id}','WordrequestController@deleteWordrequest');
});