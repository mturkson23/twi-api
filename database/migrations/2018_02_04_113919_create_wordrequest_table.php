<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordrequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_wordrequest', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('word', 150);
            $table->text('translation');
            $table->string('photourl', 50)->nullable();
            $table->ipAddress('ipaddr')->nullable();
            $table->text('locdata')->nullable();
            $table->bigInteger('countryid')->nullable();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_wordrequest');
    }
}
